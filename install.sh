#!/bin/bash
sudo mv /home/serveradmin/stellydev/docker /home/serveradmin/docker
sudo apt-get purge do-agent -y
curl -sSL https://repos.insights.digitalocean.com/install.sh | sudo bash
echo "=====Should have finished installing the DO agent====="
sleep 5
export EXIP=$(curl ifconfig.io)
echo 'EXIP'=""$EXIP"" >> /etc/environment
echo 'LAN'="10.0.0.0/24" >> /etc/environment
chmod 755 /etc/environment
echo "=====Should have finished creating serveradmin====="
sleep 5
sudo apt-get -y remove docker docker-engine docker.io containerd runc
sudo apt-get -y update
sudo apt-get -y upgrade
sudo apt-get -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get -y update
sudo apt-get -y install docker-ce docker-ce-cli containerd.io
sudo curl -L https://github.com/docker/compose/releases/download/1.27.4/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
echo "=====Should have finished installing Docker and Docker Compose====="
sleep 5
sudo mkdir /home/serveradmin/docker
sudo usermod -aG docker serveradmin
sudo setfacl -Rdm g:docker:rwx /home/serveradmin/docker
sudo chmod -R 775 /home/serveradmin/docker
mkdir /home/serveradmin/docker/traefik2
mkdir /home/serveradmin/docker/traefik2/acme
touch /home/serveradmin/docker/traefik2/acme/acme.json
chmod 600 /home/serveradmin/docker/traefik2/acme/acme.json
touch /home/serveradmin/docker/traefik2/traefik.log
docker network create t2_proxy
docker network create --gateway 192.168.90.1 --subnet 192.168.90.0/24 t2_proxy
sudo -E docker-compose -f /home/serveradmin/docker/docker-compose-t2.yml up -d --remove-orphans
##need to add the ability to get real certificates here
